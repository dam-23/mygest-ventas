from openpyxl import load_workbook
from openpyxl import Workbook
from numpy import ndarray
import formulas
import easyFormulas


def obtener_fecha(ruta):
    workbook = load_workbook(ruta, data_only=True)
    sheet = workbook.active
    fecha = sheet['H3'].value
    fecha_sinHora=fecha.date()
    return fecha_sinHora



def leer_archivo_excel(ruta):
    workbook = load_workbook(ruta, data_only=True)
    sheet = workbook.active
    fecha = sheet['H3'].value
    total_sin_iva = sheet['F47'].value
    iva = sheet['F48'].value
    print("La fecha es: ", fecha)
    print("El total sin IVA: ", total_sin_iva)
    print("El IVA al 21% es: ", iva)

def crear_documento(fecha_sin_hora, total_sin_iva, iva):

     workbook_ventas = Workbook()

     worksheet_ventas = workbook_ventas.active

     worksheet_ventas.title = 'Ventas'

     # Agregar informacion al archivo excel

     worksheet_ventas.cell(row=1, column=1, value="Fecha")
     worksheet_ventas.cell(row=2, column=1, value="Total Sin IVA")
     worksheet_ventas.cell(row=3, column=1, value="IVA")

     worksheet_ventas.cell(row=1, column=2, value=fecha_sin_hora)
     worksheet_ventas.cell(row=2, column=2, value=total_sin_iva)
     worksheet_ventas.cell(row=3, column=2, value=iva)

     # Guardar el archivo
     nombre_archivo = "Resources/documento_ventas.xlsx"
     workbook_ventas.save(nombre_archivo)
     print(f"Se ha creado el documento de ventas '{nombre_archivo}'")



if __name__ == "__main__":
    ruta_archivo = 'Resources/factura-emitida.xlsx'
    fecha_sin_hora = obtener_fecha(ruta_archivo)
    totalSinIva=easyFormulas.EasySheet(ruta_archivo)
    sinIva=totalSinIva.get_value_of_cell("factura","F47")
    total = easyFormulas.EasySheet(ruta_archivo)
    Iva = total.get_value_of_cell("factura", "F48")
    total_sin_iva=sinIva
    iva = Iva
    print(sinIva)
    print(fecha_sin_hora)
    print(iva)

    crear_documento(fecha_sin_hora, total_sin_iva, iva)






